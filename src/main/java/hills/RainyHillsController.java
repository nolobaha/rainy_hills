package hills;

import hills.calculator.CapacityCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

/** Performs HTTP routine for serving content related to the "Rainy Hills" module. */
@RestController
public class RainyHillsController {

    @Autowired
    private CapacityCalculator capacityCalculator;

    @RequestMapping(value = "/hills/{hills}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Long> calculateCapacity(@PathVariable int[] hills) {
        return Collections.singletonMap("result", capacityCalculator.calculateCapacity(hills));
    }
}
