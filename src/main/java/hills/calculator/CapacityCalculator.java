package hills.calculator;

/** Service to calculate volume of water that is piled up between mountain hills. */
public interface CapacityCalculator {

    /** Calculates volume on given mountain interval. */
    long calculateCapacity(int[] array);
}
