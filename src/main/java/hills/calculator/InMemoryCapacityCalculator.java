package hills.calculator;

/** Implementation of {@link CapacityCalculator} that performs all calculations in memory. */
public class InMemoryCapacityCalculator implements CapacityCalculator {

    /**
     * {@inheritDoc}
     * The algorithm works as follows:
     * we set two pointers to the beginning and ending of the array and start bring them together one unit per
     * iteration. If there is a difference in levels on the both ends then we start looking for a hollow on each
     * side. If level of the hollow is lower than level of the previous unit at the corresponding end
     * then its a place where water gets piled up.
     *
     * Time complexity: O(n)
     * Space complexity: O(n)
     */
    @Override
    public long calculateCapacity(int[] array) {
        long sum = 0;
        int lowIndex = 0;
        int highIndex = array.length - 1;
        int leftLevel = 0;
        int rightLevel = 0;
        while (lowIndex <= highIndex) {
            if (array[lowIndex] < array[highIndex]) {
                if (array[lowIndex] < leftLevel) {
                    // we have a hollow so need to accumulate sum
                    sum = sum + (leftLevel - array[lowIndex]);
                } else {
                    leftLevel = array[lowIndex];
                }
                lowIndex++;
            } else {
                if (array[highIndex] < rightLevel) {
                    // we have a hollow so need to accumulate sum
                    sum = sum + (rightLevel - array[highIndex]);
                } else {
                    rightLevel = array[highIndex];
                }
                highIndex--;
            }
        }
        return sum;
    }
}
