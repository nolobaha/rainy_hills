package hills.config;

import hills.calculator.CapacityCalculator;
import hills.calculator.InMemoryCapacityCalculator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/** Module configuration. */
@Configuration
public class AppConfig {

    @Bean
    public CapacityCalculator calculator() {
        return new InMemoryCapacityCalculator();
    }
}
