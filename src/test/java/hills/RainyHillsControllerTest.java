package hills;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import hills.calculator.CapacityCalculator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RainyHillsApplication.class)
@AutoConfigureMockMvc
public class RainyHillsControllerTest {

    @Autowired private MockMvc mvc;

    @Autowired WebApplicationContext webApplicationContext;

    @MockBean CapacityCalculator capacityCalculator;

    @Test
    public void calculateCapacityTest() throws Exception {
        String uri = "/hills/1,2,3";
        when(capacityCalculator.calculateCapacity(any())).thenReturn(1L);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        Map<String, Object> resultMap = parseJson(mvcResult.getResponse().getContentAsString());
        assertEquals(1, resultMap.get("result"));
    }

    @Test
    public void calculateCapacityInvalidArgumentsTest() throws Exception {
        String uri = "/hills/something-bad";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
    }

    private Map<String, Object> parseJson(String jsonContent) throws Exception {
        return new ObjectMapper().readValue(jsonContent, new TypeReference<Map<String, Object>>(){});
    }
}
