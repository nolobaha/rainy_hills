package hills.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(BlockJUnit4ClassRunner.class)
public class InMemoryCapacityCalculatorTest {

    @Test
    public void calcTestOneHollow() {
        assertEquals(2, new InMemoryCapacityCalculator().calculateCapacity(new int[] {3, 1, 3}));
    }

    @Test
    public void calcTestTwoHollows() {
        assertEquals(4, new InMemoryCapacityCalculator().calculateCapacity(new int[] {3, 1, 3, 4, 2, 8}));
    }

    @Test
    public void calcSinkTest() {
        assertEquals(0, new InMemoryCapacityCalculator().calculateCapacity(new int[] {1, 2, 3, 4}));
    }

    @Test
    public void calcTwoWaySinkTest() {
        assertEquals(0, new InMemoryCapacityCalculator().calculateCapacity(new int[] {1, 2, 3, 4, 3, 2, 1}));
    }

    @Test
    public void calcExampleTest() {
        assertEquals(2, new InMemoryCapacityCalculator().calculateCapacity(new int[] {3, 2, 4, 1, 2}));
    }

    @Test
    public void calcExampleTest2() {
        assertEquals(8, new InMemoryCapacityCalculator().calculateCapacity(new int[] {4, 1, 1, 0, 2, 3}));
    }

    @Test
    public void calcTest1() {
        assertEquals(4, new InMemoryCapacityCalculator().calculateCapacity(new int[] {2, 0, 1, 3, 2, 3}));
    }

    @Test
    public void calcTest2() {
        assertEquals(3, new InMemoryCapacityCalculator().calculateCapacity(new int[] {1, 0, 1, 2, 3, 1, 0, 1, 0, 1}));
    }

    @Test
    public void calcTest3() {
        assertEquals(3, new InMemoryCapacityCalculator().calculateCapacity(new int[] {3, 2, 1, 0, 2}));
    }

    @Test
    public void calcTest4() {
        assertEquals(10, new InMemoryCapacityCalculator().calculateCapacity(new int[] {4, 3, 1, 2, 0, 4}));
    }

    @Test
    public void calcTest5() {
        assertEquals(6, new InMemoryCapacityCalculator().calculateCapacity(new int[] {4, 3, 1, 2, 0, 3}));
    }

    @Test
    public void calcTest6() {
        assertEquals(1, new InMemoryCapacityCalculator().calculateCapacity(new int[] {1, 2, 3, 4, 3, 4, 3, 2, 1}));
    }

    @Test
    public void calcTest7() {
        assertEquals(10, new InMemoryCapacityCalculator().calculateCapacity(new int[] {4, 1, 1, 0, 2, 3, 2, 0, 2}));
    }

    @Test
    public void calcTest8() {
        assertEquals(14, new InMemoryCapacityCalculator().calculateCapacity(new int[] {4, 1, 1, 0, 2, 3, 2, 0, 2, 2, 3}));
    }

    @Test
    public void calcTest9() {
        assertEquals(7, new InMemoryCapacityCalculator().calculateCapacity(new int[] {4, 1, 1, 0, 2, 1, 2, 0, 2, 2, 2}));
    }
}
