# Rainy Hills Service

How to start the application
---

1. Run `mvn clean install` to build your application
2. Start application with `java -jar target/rainy-hills-0.0.1-SNAPSHOT.jar`
3. Send request in the format of `http://localhost:8080/hills/{hills}` where "{hills}" is a comma-separated list of
elements that define the height of hills. Please check examples below:

```bash
curl -X GET http://localhost:8080/hills/3,2,4,1,2
```

or

```bash
curl -X GET http://localhost:8080/hills/4,1,1,0,2,3
```
